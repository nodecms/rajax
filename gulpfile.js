const gulp = require('gulp')
const uglify = require('gulp-uglify')
const ts = require('gulp-typescript')
const obs = require('gulp-javascript-obfuscator')
const tsConfig = require('./tsconfig.json').compilerOptions
const uglifyConfig = {
    mangle: {
        eval: true,
        properties: false,
        reserved: true,
        toplevel: false,
        keep_fnames: false
    },
    output: {
        braces: false,
        comments: "some",
        semicolons: true,
        webkit: true,
        beautify: false,
        quote_keys: "'",
        shebang: false,
        wrap_iife: true,
        ascii_only: true,
        max_line_len: 80
    }
}

function build () {
    return gulp.src('./src/**/*.ts')
        .pipe(ts(tsConfig))
        .pipe(uglify(uglifyConfig))
        .pipe(gulp.dest('./dist/'))
}

function watch () {
    gulp.watch(['./src/**/*.ts'], gulp.series('dev'))
}

function dev () {
    return gulp.src('./src/**/*.ts')
        .pipe(ts(tsConfig))
        .pipe(gulp.dest('./dist/'))
}

module.exports = {
    build,
    dev,
    watch
}