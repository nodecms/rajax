
$(document).ready(() => {
    const forms: JQuery<HTMLFormElement> = $('form[data-najax]')
    const w: any = window

    forms.submit(function (e) {
        const form = this
        const button = $(this).find(':submit')

        if (button.attr('disabled')) return e.preventDefault()

        const formData = new FormData(form)
        const actionURL: string | undefined = $(form).data('action')
        const method: string | undefined = $(form).attr('method')
        const enctype: string | undefined = $(form).attr('enctype')
        const timeout = $(form).data('timeout')

        if (formData && actionURL && method) {
            e.preventDefault()

            button.attr('disabled', 'disabled')

            const ajax_opts: JQueryAjaxSettings = {
                url: actionURL,
                data: formData,
                method: method,
                cache: false,
                timeout,
                xhr: function () {
                    var xhr = new window.XMLHttpRequest()

                    xhr.upload.addEventListener("progress", function (evt) {
                        const uploadProgress = $(form).attr('data-upload-progress')
                        $(form).trigger('najax-upload-progress', [evt.loaded / evt.total])
                        if (uploadProgress)
                            if (w[uploadProgress])
                                w[uploadProgress](evt.loaded / evt.total)
                            else
                                console.error('Cannot call `' + uploadProgress + '` function.')
                    }, false)

                    xhr.addEventListener("progress", function (evt) {
                        const downloadProgress = $(form).attr('data-download-progress')
                        $(form).trigger('najax-download-progress', [evt.loaded / evt.total])
                        if (downloadProgress)
                            if (w[downloadProgress])
                                w[downloadProgress](evt.loaded / evt.total)
                            else
                                console.error('Cannot call `' + downloadProgress + '` function.')
                    }, false)

                    return xhr
                },
                headers: {
                    'Accept': 'application/json'
                },
                beforeSend: () => {
                    const beforeSend = $(form).attr('data-before-send')

                    $(form).trigger('najax-before-send')

                    if (beforeSend)
                        if (w[beforeSend])
                            w[beforeSend]()
                        else
                            console.error('Cannot call `' + beforeSend + '` function.')
                },
                processData: false,
                contentType: false,
                enctype: enctype == 'multipart/form-data' || enctype === 'application/x-www-form-urlencoded' || enctype === 'text/plain' ? enctype : 'application/x-www-form-urlencoded'
            }

            $.ajax(ajax_opts)
                .done((res) => {

                    if (res && Array.isArray(res) && res.length > 0) {
                        const onSuccess = $(form).attr('data-on-success')
                        const success = res[0].message

                        $(form).trigger('najax-success', [success])

                        if (onSuccess)
                            if (w[onSuccess])
                                w[onSuccess](success)
                            else
                                console.error('Cannot call `' + onSuccess + '` function.')
                        return
                    }

                    $(form).submit()
                })
                .fail((res) => {
                    if (res && res.responseJSON && res.responseJSON && Array.isArray(res.responseJSON) && res.responseJSON.length > 0) {
                        const onError = $(form).attr('data-on-error')
                        const err = res.responseJSON[0].message

                        $(form).trigger('najax-error', [err])

                        if (onError)
                            if (w[onError])
                                w[onError](err)
                            else
                                console.error('Cannot call `' + onError + '` function.')
                        return
                    }

                    $(form).submit()
                })
                .always(() => {
                    button.removeAttr('disabled')

                    const afterSend = $(form).attr('data-after-send')

                    $(form).trigger('najax-after-send')

                    if (afterSend)
                        if (w[afterSend])
                            w[afterSend]()
                        else
                            console.error('Cannot call `' + afterSend + '` function.')
                })
        } else {
            if (!actionURL) {
                console.error('Missing property `data-action` in the form tag.')
            }

            if (!method) {
                console.error('Missing property `method` in the form tag.')
            }

            if (!formData) {
                console.error('FormData cannot be created.')
            }
        }
    })
})