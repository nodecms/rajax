# NAjax

NAjax permet d'envoyer les formulaire directement en ajax en rajoutant un attribut sur le formulaire.

## Comment l'utiliser ?

Ajoutez le fichier `dist/main.js` ansi que jQuery dans l'élement `<head>` de votre page :

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/path/to/main.js"></script>
```

Créez votre formulaire et ajoutez-y un attribut `data-najax` sans aucune valeur :

```html
<form data-action="http://sample.com/post_form" method="post" data-najax>
    <input type="text" name="sample_input" />

    <button type="submit">submit</button>
</form>
```

Dans votre formulaire, les attributs `data-action`, et `method` sont obligatoire. NodeCMS-Ajax-API va regarder ces attributs pour faire la requête. Si vous souhaitez uploader un fichier, vous devez renseigner l'attribut `enctype="multipart/form-data"`.

## Les callbacks

| Nom de l'attribut      | Valeur                              |
| ---------------------- | ----------------------------------- |
| data-before-send       | Function: () => void                |
| data-upload-progress   | Function: (percent: number) => void |
| data-download-progress | Function: (percent: number) => void |
| data-on-error          | Function: (err: string) => void     |
| data-on-success        | Function: (success: string) => void |
| data-after-send        | Function: () => void                |

Pour ajouter un callback vous avez juste à ajouter un attribut dans l'élement `<form>` voulu et lui passer une fonction javascript. Exemple de définission de callback :

```html
<form
    data-action="http://sample.com/post_form"
    method="post"
    data-on-error="onError"
    data-najax
>
    <input type="text" name="sample_input" />

    <button type="submit">submit</button>
</form>

<script>
    function onError(err) {
        alert("Il y a eu une erreur...");
    }
</script>
```

## Les callbacks en jQuery

| Nom de l'évènement      | Valeur                                 |
| ----------------------- | -------------------------------------- |
| najax-before-send       | Function: (e) => void                  |
| najax-upload-progress   | Function: (e, percent: number) => void |
| najax-download-progress | Function: (e, percent: number) => void |
| najax-error             | Function: (e, err: string) => void     |
| najax-success           | Function: (e, success: string) => void |
| najax-after-send        | Function: (e) => void                  |

Pour utiliser les callbacks en jQuery vous devez faire `$('#formulaire').on('event', function() {})` :

```html
<form
    data-action="http://sample.com/post_form"
    method="post"
    data-on-error="onError"
    id="formulaire"
    data-najax
>
    <input type="text" name="sample_input" />

    <button type="submit">submit</button>
</form>

<script>
    $("#formulaire").on("najax-error", function (e, err) {
        alert("Il y a eu une erreur...");
    });
</script>
```
