const express = require('express')
const path = require('path')
const app = express()
const bodyParser = require('body-parser')
const multiparty = require('multiparty')

app.use('/dist', express.static(__dirname + '/dist'))

app.use(bodyParser.json())       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}))

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/api', function (req, res) {
    res.status(200)
    return res.json([{ message: 'mesdfsage d\'success' }])
})

app.listen(80, function () {
    console.log('Example app listening on port 80!')
})