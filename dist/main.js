"use strict";$(document).ready(function(){var r=$("form[data-najax]"),c=window
;r.submit(function(r){var e=this,t=$(this).find(":submit");if(t.attr("disabled")
)return r.preventDefault();var a=new FormData(e),o=$(e).data("action"),n=$(e
).attr("method"),s=$(e).attr("enctype"),i=$(e).data("timeout");if(a&&o&&n){
r.preventDefault(),t.attr("disabled","disabled");var d={"url":o,"data":a,
"method":n,"cache":!1,"timeout":i,"xhr":function(){
var r=new window.XMLHttpRequest;return r.upload.addEventListener("progress",
function(r){var t=$(e).attr("data-upload-progress");$(e).trigger(
"najax-upload-progress",[r.loaded/r.total]),t&&(c[t]?c[t](r.loaded/r.total
):console.error("Cannot call `"+t+"` function."))},!1),r.addEventListener(
"progress",function(r){var t=$(e).attr("data-download-progress");$(e).trigger(
"najax-download-progress",[r.loaded/r.total]),t&&(c[t]?c[t](r.loaded/r.total
):console.error("Cannot call `"+t+"` function."))},!1),r},"headers":{
"Accept":"application/json"},"beforeSend":function(){var r=$(e).attr(
"data-before-send");$(e).trigger("najax-before-send"),r&&(c[r]?c[r](
):console.error("Cannot call `"+r+"` function."))},"processData":!1,
"contentType":!1,
"enctype":"multipart/form-data"==s||"application/x-www-form-urlencoded"===s||"text/plain"===s?s:"application/x-www-form-urlencoded"
};$.ajax(d).done(function(r){if(r&&Array.isArray(r)&&0<r.length){var t=$(e
).attr("data-on-success"),a=r[0].message;return $(e).trigger("najax-success",[a]
),void(t&&(c[t]?c[t](a):console.error("Cannot call `"+t+"` function.")))}$(e
).submit()}).fail(function(r){if(
r&&r.responseJSON&&r.responseJSON&&Array.isArray(r.responseJSON
)&&0<r.responseJSON.length){var t=$(e).attr("data-on-error"),
a=r.responseJSON[0].message;return $(e).trigger("najax-error",[a]),void(t&&(
c[t]?c[t](a):console.error("Cannot call `"+t+"` function.")))}$(e).submit()}
).always(function(){t.removeAttr("disabled");var r=$(e).attr("data-after-send")
;$(e).trigger("najax-after-send"),r&&(c[r]?c[r]():console.error(
"Cannot call `"+r+"` function."))})}else o||console.error(
"Missing property `data-action` in the form tag."),n||console.error(
"Missing property `method` in the form tag."),a||console.error(
"FormData cannot be created.")})});